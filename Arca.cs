﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using INI;
//using System.Collections;
//using System.Reflection;
//using System.Diagnostics;
//using System.Windows.Forms;


namespace myArcaNameSpace
{
    
    public static class CONSTANTS
    {
        public const string status = "1B76";
        public const string printerIdendity = "1B49";
        public const string printerLineFeed = "0A";
        public const string printerCancelBuffer = "18";
        public const string printerPartialCut = "1B6D";
        public const string printerFullCut = "1B69";
    }
    public class CC3R
    {
        public const string LCSET = "#";
        const string LCGET = "$";
        

        public CC3R()
        {
            
        }

        public struct SPrinterCmd
        {
            
        }
        

        public struct SCC3R
        {
            public string productCode;
            public string productSerialNumber;
            public SSideKeypad sideKeypad;
            public SCoinHopper hopper;
        }
        public SCC3R System;

        public struct SSideKeypad
        {
            public string description;
            public string firwmare;
        }

        


        public string DconCommand(string cmd)
        {
            string command = "";
            string chk = "";
            int temp = 0;
            for (int i = 0; i < cmd.Length; i++)
            {
                command += CM18tool.Arca.Ascii2Hex(cmd.Substring(i, 1)).ToUpper();

            }
            for (int i = 0; i < command.Length; i = i + 2)
            {
                temp += Convert.ToInt16(CM18tool.Arca.Hex2Dec(command.Substring(i, 2)));
            }
            chk = CM18tool.Arca.Dec2Hex(temp);
            chk = CM18tool.Arca.Ascii2Hex(chk.Substring(chk.Length-2, 1)) + CM18tool.Arca.Ascii2Hex(chk.Substring(chk.Length - 1, 1));
            command += chk.Substring(chk.Length - 4, 4);
            command += "0D";
            return command;
        }
        public string DconCommand(string leadChr,int moduleAddress, string cmd)
        {
            cmd = leadChr + CM18tool.Arca.Dec2Hex(moduleAddress, 2) + cmd;
            string command = "";
            string chk = "";
            int temp = 0;
            for (int i = 0; i<cmd.Length;i++)
            {
                command += CM18tool.Arca.Ascii2Hex(cmd.Substring(i, 1)).ToUpper();

            }
            for (int i = 0; i<command.Length;i=i+2)
            {
                temp += Convert.ToInt16(CM18tool.Arca.Hex2Dec(command.Substring(i, 2)));
            }
            chk += CM18tool.Arca.Ascii2Hex(CM18tool.Arca.Dec2Hex(temp, 2).Substring(0, 1)) + CM18tool.Arca.Ascii2Hex(CM18tool.Arca.Dec2Hex(temp, 2).Substring(1, 1));
            command += chk.Substring(chk.Length-4,4);
            command += "0D";
            return command;
        }

        public struct SRemoteIOModule
        {
            public string module;
            public string address;
            public string firmware;
            public string commProt;
            public SRemotePort[] port;
        }
        public SRemoteIOModule remoteModule;

        public struct SRemotePort
        {
            public string description;
            public string value;
            public string status;
        }

        public struct SHopper
        {
            public int address;
            public string equipCategory;
            public string fwUpdateCapability;
            public string manufacturerId;
            public string denomination;
            public string diameter;
            public string productCode;
            public string serialNumber;
            public string softwareVersion;
            public int diameter1;
            public int diameter2;
            public int diameter3;
            public int diameter4;
            public int diameter5;
            public int diameter6;
        }

        public struct SCoinHopper
        {
            public SHopper[] dxRail;
            public SHopper[] sxRail;
        }

        public struct SDConAnswer
        {
            public string lead;
            public string moduleAddress;
            public string data;
            public string chk;
            public string cr;
        }
        public SDConAnswer DconAnswer(string answer)
        {
            SDConAnswer commandAnswer = new SDConAnswer();
            if (answer.Length > 0)
            {
                commandAnswer.lead = CM18tool.Arca.Hex2Ascii(answer.Substring(0, 2));
                commandAnswer.moduleAddress = CM18tool.Arca.Hex2Ascii(answer.Substring(2, 4));
                commandAnswer.data = CM18tool.Arca.Hex2Ascii(answer.Substring(6, answer.Length - 12));
                commandAnswer.chk = CM18tool.Arca.Hex2Ascii(answer.Substring(answer.Length - 6, 2)) + CM18tool.Arca.Hex2Ascii(answer.Substring(answer.Length - 4, 2));
                commandAnswer.cr = CM18tool.Arca.Hex2Ascii(answer.Substring(answer.Length - 2, 2));
            }
            return commandAnswer;
        }

        public struct DconProtocol
        {
            public string leadChr;
            public ModuleAddress module;
            public Command command;
            public string checksum;
            public byte endCommand;
        }

        public enum LeadingCharacter : int { cc,ssw}

        public struct ModuleAddress
        {

        }

        public struct Command
        {

        }

        public struct SCCTalkProtocol
        {
            public string destinationAddress;
            public int bytes;
            public string sourceAddress;
            public string header;
            public string checksum;
            public string data;
        }

        public struct SCCTalkAnswer
        {
            public string destinationAddress;
            public int bytes;
            public string sourceAddress;
            public string header;
            public string checksum;
            public string data;
            public string answer;
        }

        public SCCTalkAnswer CcTalkAnswer(string answer)
        {
            SCCTalkAnswer commandAnswer = new SCCTalkAnswer();
            if (answer.Length > 0)
            {
                try
                {
                    commandAnswer.destinationAddress = CM18tool.Arca.Hex2Ascii(answer.Substring(0, 2));
                    commandAnswer.bytes = Convert.ToInt16(answer.Substring(2, 2));
                    commandAnswer.sourceAddress = answer.Substring(4, 2);
                    commandAnswer.header = answer.Substring(6, 2);
                    commandAnswer.checksum = answer.Substring(8, 2);
                    commandAnswer.answer = answer.Substring(10, answer.Length - 10);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return commandAnswer;
        }
        public string CcTalkCommand(int destination, int bytes, int source, string cmd, string parameter="")
        {
            string[] command = new string[4];
            string chk = "";
            int temp = 0;
            command[0] = CM18tool.Arca.Dec2Hex(destination, 2);
            command[1] = CM18tool.Arca.Dec2Hex(bytes, 2);
            command[2] = CM18tool.Arca.Dec2Hex(source, 2);
            command[3] = cmd;
            for (int i = 0; i < command.Length; i++)
            {
                temp += Convert.ToInt16(CM18tool.Arca.Hex2Dec(command[i]));
            }
            temp = 256 - temp;
            chk=CM18tool.Arca.Dec2Hex(temp, 2);
            cmd = command[0] + command[1] + command[2] + command[3] + chk;
            return cmd;
        }

    }

    #region Eccezioni 
    public class CMConnectionTypeException: Exception
    {
        public CMConnectionTypeException()
        { }

        public CMConnectionTypeException(string message)
            :base (message)
        { }
    }
    #endregion
}



namespace CM18tool
{
    public class Arca
    {
        public static int keyCodePressed = 0;
        public static IniFile iniMessage;
        public static Color messageColor = Color.FromArgb(255, 0, 0, 0); //0x00000000
        public static Color errorColor = Color.FromArgb(255, 255, 0, 0); //0x00FF0000
        public static Color warningColor = Color.FromArgb(255, 255, 184, 0);//0x00FFB800
        public static Color actionColor = Color.FromArgb(255, 0, 0, 255);//0x000000FF
        public static Color correctColor = Color.FromArgb(255, 154, 205, 50);//0x009ACD32
        public static Color waitingColor = Color.FromArgb(255, 154, 205, 50);//0x009ACD32
        public static Color arcaBlackColor = Color.FromArgb(255, 88, 89, 91);
        public static Color arcaLogoColor = Color.FromArgb(255, 85, 22, 110);
        public static Color arcaOemColor = Color.FromArgb(255, 174, 191, 55);
        public static Color arcaRetailColor = Color.FromArgb(255, 228, 86, 32);
        public static Color arcaFinancialColor = Color.FromArgb(255, 26, 50, 129);

        public Arca()
        {
            //public static STower myTower = new STower();
        }
        

        public static System.Drawing.Color ArcaColors(uint argbValue)
        {
            return System.Drawing.Color.FromArgb(Convert.ToInt16(argbValue));
        }

        public struct SSingleTest
        {
            public string description;
            public string result;
            public string logID;
            public string functionName;
            public int testID;
        }

        public struct SMyTeller
        {
            public string partNumber;
            public string description;
            public string monitorModel;
            public string cardreaderModel;
            public string printerModel;
            public string printerA4Model;
            public string EPPModel;
            public string scannerModel;
            public string scannerA4Model;
            public string webcamModel;
            public string barcodereaderModel;
            public string NFCModel;
            public string AudioTest;
            public string UPSModel;
            public string CMModel;
            public string coinModel;
            public string serialNumber;
        }

        public struct SLowerModule
        {
            public string productNumber;
            public string clientName;
            public int clientID;
            public SCd80[] cd80;
            public string fwCd80Result;
            public string cd80TestResult;
            public string suiteCode;
            public string startData;
            public string startTime;
            public string endData;
            public string endTime;
            public string assignResult;
            public string photoAdjResult;
            public string cassetteNumberResult;
            public int cassetteNumber;
            public string cassetteSetup;
            public string depositResult;
            public string testResult;
            public string catDescription;
            public string category;
            public int crmNum;
            public int cd80Num;
            public SCd80Status cd80Status;
            public SCassette[] cassettes;
        } 

        public struct SCd80
        {
            public string serialNumber;
            public string fw;
            public SPhoto[] photo;
            public string testResult;
            public string capacity;

        }
        public struct SCd80Status
        {
            public bool swLockA;
            public bool swLockB;
            public bool tray;
            public string statusA;
            public string statusB;
            public bool swMagA;
            public bool swMagB;
            public bool sorterForkA;
            public bool sorterForkB;
        }
        public struct ConnectionParameter
        {
            public string connection;
            public string simplified;
            public string Name;
            public int baudrate;
            public StopBits stopBit;
            public Parity parity;
            public int dataBits;
            public string protocol;
            public string ipAddress;
            public string hostName;
            public int port;
        }
        public ConnectionParameter myConnectionPar;

        public struct SSystem
        {
            public string partNumber;
            public string clientName;
            public string clientID;
            public string family;
            public string description;
            public string serialNumber;
            public string protInterface;
            public int protCassetteNumber;
            public string suiteCode;
            public int CRM;
            public int cassetteNumber;
            public int cassetteInstalled;
            public int cd80Number;
            public int cd80TrayNumber;
            public SUnitConfig cmConfig;
            public SUnitConfig cmOption;
            public SUnitConfig cmOption1;
            public SUnitConfig cmOption2;
            public SCassette[] cassettes;
            public SCashData cashData;
            public SBag bag;
            public SReader reader;
            public SVersion version;
            public SRealTimePhoto realtimePhoto;
            public SErrorLog[] errorLog;
        }
        public SSystem mySystem;
            
        public struct SJamErrorLog
        {
            public List<SJamDetail> jamDetail;
            public int totalErrors;
            public int feedErrors;
            public int controllerErrors;
            public int readerErrors;
            public int safeErrors;
            public int cassetteAErrors;
            public int cassetteBErrors;
            public int cassetteCErrors;
            public int cassetteDErrors;
            public int cassetteEErrors;
            public int cassetteFErrors;
            public int cassetteGErrors;
            public int cassetteHErrors;
            public int cassetteIErrors;
            public int cassetteJErrors;
            public int cassetteKErrors;
            public int cassetteLErrors;
        }

        public struct SJamDetail
        {
            public int line;
            public string moduleStatus;
            public string module;
        }
        public struct SCashData
        {
            public Int32 bnInCash;
            public SCassette[] cassette;
        }

        public struct SCassette
        {
            public string noteId;
            public Int32 noteNumber;
            public Int32 freeCapacity;
            public string name;
            public string enabled;
            public string serial;
            public SFirmware fw;
            public SPhoto[] photo;
        }

        public struct SErrorLog
        {
            public Int32 recordNumber;
            public Int32 life;
            public string opSide;
            public string opId;
            public string replyCode;
            public string F1Status;
            public string F2Status;
            public string F3Status;
            public string F4Status;
            public string FAStatus;
            public string FBStatus;
            public string FCStatus;
            public string FDStatus;
            public string FEStatus;
            public string FFStatus;
            public string FGStatus;
            public string FHStatus;
            public string FIStatus;
            public string FJStatus;
            public string FKStatus;
            public string FLStatus;
            public string FMStatus;
            public string FNStatus;
            public string FOStatus;
            public string FPStatus;
            public string line;
        }
        public struct SRealTimePhoto
        {
            public SPhoto inCenter;
            public SPhoto inLeft;
            public SPhoto hMax;
            public SPhoto feed;
            public SPhoto c1;
            public SPhoto shift;
            public SPhoto inq;
            public SPhoto count;
            public SPhoto Out;
            public SPhoto c3;
            public SPhoto c4a;
            public SPhoto c4b;
            public SPhoto rej;
            public SPhoto inBox;
            public SPhoto res1;
            public SPhoto res2;
            public SPhoto res3;
            public SPhoto res4;
            public SPhoto res5;
            public SPhoto res6;
            public SPhoto res7;
            public SPhoto cashLeft;
            public SPhoto cashRight;
            public SPhoto thick;
            public SPhoto finrcyc;
            public SPhoto res10;
            public SPhoto res11;
            public SPhoto res12;
            public SPhoto res13;
            public SPhoto res14;
            public SPhoto res15;
            public SPhoto res16;
            public SPhoto res17;
            public SPhoto res18;
            public SPhoto res19;
            public SPhoto res20;
            public SPhoto res21;
            public SPhoto res22;
            public SPhoto res23;
            public SPhoto res24;
            public SPhoto res25;
        }
        public struct SVersion
        {
            public SFirmware compatibility;
            public SFirmware oscBoot;
            public SFirmware oscWce;
            public SFirmware oscxDll;
            public SFirmware oscxApp;
            public SFirmware fpga;
            public SFirmware Controller;
            public SFirmware Controller2;
            public SFirmware readerHost;
            public SFirmware readerDsp;
            public SFirmware readerFpga;
            public SFirmware readerTape;
            public SFirmware readerMagnetic;
            public SFirmware safe;
            public SFirmware cassetteA;
            public SFirmware cassetteB;
            public SFirmware cassetteC;
            public SFirmware cassetteD;
            public SFirmware cassetteE;
            public SFirmware cassetteF;
            public SFirmware cassetteG;
            public SFirmware cassetteH;
            public SFirmware cassetteI;
            public SFirmware cassetteJ;
            public SFirmware cassetteK;
            public SFirmware cassetteL;
            public SFirmware bagContrA;
            public SFirmware bagSensorA;


        }

        public struct SFirmware
        {
            public string name;
            public string id;
            public string partNumber;
            public string release;
            public string version;
        }

        public struct SReader
        {
            public string serial;
            public string family;
            public string fwCode;
            public string cdfCode;
            public string cdfName;
            public string currencyMode;
            public SBankCfg[] bankCfg;
            public SCurrency[] currency;
        }

        public struct SBankCfg
        {
            public string name;
            public string enabled;
            public string bankId;
        }
        public struct SCurrency
        {
            public string name;
            public string enabled;
            public string bankId;
            public string version;
        }
        public struct SBag
        {
            public string name;
            public string fwVer;
            public string fwRes;
            public string serial;
            public string denomination;
        }

        public struct SUnitConfig
        {
            public SBitUnitConfig[] bit;
        }
        public SUnitConfig CMConfig;

        public struct SBitUnitConfig
        {
            public string address;
            public string description;
            public bool value;
        }
        public struct SHCLow
        {
            public string moduleInError;
            public string moduleError;
            public string towerInTest;
            //public string towerSN;
            //public string towerTL;
            public SPhoto phInA;
            public SPhoto phInB;
            public STower[] tower;
        }
        public SHCLow[] myHcLow;

        public struct STower
        {
            public string status;
            public int idTower;
            public string fwRel;
            public string fwVer;
            public string name;
            public string serialNumber;
            public string snBoard;
            public string mechanicalLevel;
            public string electronicLevel;
            public string technicalLevel;
            public int motorCurrent;
            public string type;
            public SElm sorter;
            public SDrum[] drum;
            public SPhoto phTr1;

        }
        public STower myTower;

        public struct SDrum
        {
            public string status;
            public string name;
            public string address;
            public string denomination;
            public int bnHeight;
            public int bnNumber;
            public string fwVer;
            public string fwRel;
            public string serialNumber;
            public string snHigh;
            public string snMid;
            public string snLow;
            public string snBoard;
            public string mechanicalLevel;
            public string electronicLevel;
            public string technicalLevel;
            public int motorCurrent;
            public int windTapeLenght;
            public int unwindTapeLenght;
            public int tapeLenghtDiff;
            public SElm pinchRoller;
            public SElm sorter;
            public SPhoto phIn;
            public SPhoto phEmpty;
            public SPhoto phFull;
            public SPhoto phFill;
        }
        public SDrum myDrum;

        public struct SSafe
        {
            public string fwRel;
            public string fwVer;
            public string serialNumber;
            public string snBoard;
            public string state;
            public string doorSim;
            public string swDoorMec;
            public string swDoorEl;
            public string mechanicalLevel;
            public string electronicLevel;
            public int motorCurrent;
            public SElm sorter;
            public SPhoto phInA;
            public SPhoto phInB;
            public SPhoto phTr2;
            public SPhoto phTr3;
        }

        public struct SPhoto
        {
            public string name;
            public string idPar;
            public int value;
            public int adjMin;
            public int adjMax;
            public string adjResult;
        }
        public SPhoto myPhoto;

        public struct SElm
        {
            public string name;
            public string idPar;
            public int openingTime;
            public int closingTime;
            public int currentValue;
        }
        public SElm myElm;



        #region HEX / BIN / DEC convertions
        /// <summary>
        /// Convert a HEX value to a DEC value
        /// </summary>
        /// <param name="value">HEX value</param>
        /// <returns></returns>
        public static int Hex2Dec(string value)
        {
            value = value.ToUpper();
            int result = 0;
            string temp = "";
            int molt = 0;
            if (value != "NO ANSWER")
            {
                for (int i = 1; i <= value.Length; i++)
                {
                    temp = value.Substring(value.Length - i, 1);
                    switch (temp)
                    {
                        case "A":
                            molt = 10;
                            break;
                        case "B":
                            molt = 11;
                            break;
                        case "C":
                            molt = 12;
                            break;
                        case "D":
                            molt = 13;
                            break;
                        case "E":
                            molt = 14;
                            break;
                        case "F":
                            molt = 15;
                            break;
                        default:
                            molt = Convert.ToInt16(temp);
                            break;
                    }
                    result += Convert.ToInt16(Math.Pow(16, i - 1) * molt);
                }
            }
            return result;
        }

        public static UInt32 Hex2Uint(string value)
        {
            value = value.ToUpper();
            uint result = 0;
            string temp = "";
            uint molt = 0;
            if (value != "NO ANSWER")
            {
                for (int i = 1; i <= value.Length; i++)
                {
                    temp = value.Substring(value.Length - i, 1);
                    switch (temp)
                    {
                        case "A":
                            molt = 10;
                            break;
                        case "B":
                            molt = 11;
                            break;
                        case "C":
                            molt = 12;
                            break;
                        case "D":
                            molt = 13;
                            break;
                        case "E":
                            molt = 14;
                            break;
                        case "F":
                            molt = 15;
                            break;
                        default:
                            molt = Convert.ToUInt32(temp);
                            break;
                    }
                    result += Convert.ToUInt32(Math.Pow(16, i - 1) * molt);
                }
            }
            return result;

        }
        public static double Hex2Dbl(string value)
        {
            value = value.ToUpper();
            double result = 0;
            string temp = "";
            double molt = 0;
            if (value != "NO ANSWER")
            {
                for (int i = 1; i <= value.Length; i++)
                {
                    temp = value.Substring(value.Length - i, 1);
                    switch (temp)
                    {
                        case "A":
                            molt = 10;
                            break;
                        case "B":
                            molt = 11;
                            break;
                        case "C":
                            molt = 12;
                            break;
                        case "D":
                            molt = 13;
                            break;
                        case "E":
                            molt = 14;
                            break;
                        case "F":
                            molt = 15;
                            break;
                        default:
                            molt = Convert.ToInt16(temp);
                            break;
                    }
                    result += Convert.ToDouble(Math.Pow(16, i - 1) * molt);
                }
            }
            return result;
        }
        /// <summary>
        /// Convert a HEX value to a BIN value
        /// </summary>
        /// <param name="value">HEX value</param>
        /// <param name="bit">Number of bit (considered only if highter of number of bit calculated)</param>
        /// <returns></returns>
        public static string Hex2Bin(string value,int bit=0)
        {
            value = value.ToUpper();
            string result = "";
            string temp = "";
            if (value != "NO ANSWER")
            {
                for (int i = 1; i <= value.Length; i++)
                {
                    temp = value.Substring(i - 1, 1);
                    switch (temp)
                    {
                        case "0":
                            temp = "0000";
                            break;
                        case "1":
                            temp = "0001";
                            break;
                        case "2":
                            temp = "0010";
                            break;
                        case "3":
                            temp = "0011";
                            break;
                        case "4":
                            temp = "0100";
                            break;
                        case "5":
                            temp = "0101";
                            break;
                        case "6":
                            temp = "0110";
                            break;
                        case "7":
                            temp = "0111";
                            break;
                        case "8":
                            temp = "1000";
                            break;
                        case "9":
                            temp = "1001";
                            break;
                        case "A":
                            temp = "1010";
                            break;
                        case "B":
                            temp = "1011";
                            break;
                        case "C":
                            temp = "1100";
                            break;
                        case "D":
                            temp = "1101";
                            break;
                        case "E":
                            temp = "1110";
                            break;
                        case "F":
                            temp = "1111";
                            break;
                    }
                    result += temp;
                }

                if (bit > result.Length)
                {
                    for (int i = result.Length; i < bit; i++)
                    {
                        result = "0" + result;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Convert DEC number (int) to HEX value
        /// </summary>
        /// <param name="value">DEC (int) number</param>
        /// <param name="bit">Number of bit (considered only if highter of number of bit calculated)</param>
        /// <returns></returns>
        public static string Dec2Hex(int value, int bit = 0)
        {
            string result = value.ToString("X" + bit);
            return result;
        }

        /// <summary>
        /// Convert DEC number (double) to HEX value
        /// </summary>
        /// <param name="value">DEC (double) number</param>
        /// <param name="bit">Number of bit (considered only if highter of number of bit calculated)</param>
        /// <returns></returns>
        public static string Uint2Hex(UInt32 value, int bit = 0)
        {
            string result = value.ToString("X" + bit);
            return result;

        }
        /// <summary>
        /// Convert DEC number (int) to HEX value
        /// </summary>
        /// <param name="value">DEC 8int) number</param>
        /// <param name="bit">Number of bit (considered only if highter of number of bit calculated)</param>
        /// <returns></returns>
        public static string Dec2Bin (int value, int bit = 0)
        {
            string result = Convert.ToString(value,2) ;
            if (bit > result.Length)
            {
                for (int i = result.Length;i<bit;i++)
                {
                    result = "0" + result;
                }
            }
            return result;
        }

        public static string ResulRc (string rc)
        {
            if (rc == "101" || rc =="1")
            {
                return "PASS";
            }
            return rc;
        }

        public static bool AsciiDec2Bool (string value)
        {
            if (value == "1")
                return true;
            return false;
        }

        /// <summary>
        /// Convert BIN value to a DEC number (int)
        /// </summary>
        /// <param name="value">BIN value</param>
        /// <returns></returns>
        public static int Bin2Dec (string value)
        {
            int result = 0;
            int temp = 0;

            for (int i = 0; i < value.Length; i++)
            {
                temp = Convert.ToInt16(value.Substring(value.Length - (i+1), 1));
                if (temp == 1)
                {
                    temp = Convert.ToInt16(Math.Pow(2, i));
                }
                result += temp;
            }
            
            return result;
        } 

        public static string Bin2Hex (string value, int bit = 0)
        {
            int temp = Bin2Dec(value);
            string result = Dec2Hex(temp, bit);
            return result;
        }
        
        public static int Hex2AsciiDec(string value)
        {
            int result = 0;
            if (value != "NO ANSWER")
            {
                string temp = string.Empty;
                for (int i = 0; i < value.Length; i += 2)
                {
                    string char2Convert = value.Substring(i, 2);
                    int n = Convert.ToInt32(char2Convert, 16);
                    char c = (char)n;
                    temp += c.ToString();
                }
                result = Convert.ToInt16(temp);
            }
            return result;
        }

        public static string Hex2Ascii(string value)
        {
            string result = string.Empty;
            if (value != "NO ANSWER")
            {
                for (int i = 0; i < value.Length; i += 2)
                {
                    string char2Convert = value.Substring(i, 2);
                    int n = Convert.ToInt32(char2Convert, 16);
                    char c = (char)n;
                    result += c.ToString();
                }
            }
            return result;
        }
        
        public static string Ascii2Hex (string value)
        {
            StringBuilder sb = new StringBuilder();
            byte[] inputBytes = Encoding.UTF8.GetBytes(value);
            foreach(byte b in inputBytes)
            {
                sb.Append(string.Format("{0:X2}",b));
            }
            return sb.ToString();
        }

        #endregion



        public static string WaitingKey(string specificKey = "")
        {
            System.Windows.Forms.Application.DoEvents();
            keyCodePressed = 0;
            bool pressed = false;
            int specificValue = 1000;
            string key = "";
            switch (specificKey)
            {
                case "ENTER":
                case "INVIO":
                    specificValue = 13;
                    break;
                case "ESC":
                    specificValue = 27;
                    break;
                default:
                    if (specificKey.Length>0)
                    specificValue = Convert.ToChar(specificKey);
                    break;
            }
            do
            {
                System.Windows.Forms.Application.DoEvents();
                if (specificValue == 1000 && keyCodePressed != 0)
                { pressed = true; }
                if (specificValue == keyCodePressed)
                { pressed = true; }
            } while (pressed == false);

            switch (keyCodePressed)
            {
                case 13:
                    key = "ENTER";
                    break;
                case 27:
                    key = "ESC";
                    break;
                case 32:
                    key = "SPACE";
                    break;
                case 82:
                case 114:
                    key = "R";
                    break;
                case 76:
                case 108:
                    key = "L";
                    break;
                default:
                    key = Convert.ToChar(keyCodePressed).ToString();
                    break;
            }
            keyCodePressed = 0;
            return key;
        }

        public static void WriteMessage(System.Windows.Forms.Label label,string message, Color Colors = default(Color))
        {
            label.Text = message;
            //if (Colors == default(Color)) { Colors=Color.Green; }
            label.ForeColor = Colors;
            label.Refresh();
        }

        public static void WriteMessage(System.Windows.Forms.Label label, int messageID, Color Colors = default(Color))
        {
            label.Text = iniMessage.GetValue("messages", "msg" + messageID);
            if (Colors == default(Color)) { Colors = Color.White; }
            label.ForeColor = Colors;
            label.Refresh();
        }

        public static void WriteMessage(System.Windows.Forms.Label label, int messageID, Color Colors = default(Color), params object[] param)
        {
            label.Text = string.Format(iniMessage.GetValue("messages", "msg" + messageID),param);
            //if (Colors == default(Color)) { Colors = Color.Green; }
            label.ForeColor = Colors;
            label.Refresh();
        }
        

        public static void WaitingTime(int mSec = 1000)
        {
            System.Windows.Forms.Application.DoEvents();
            System.Threading.Thread.Sleep(mSec);
        }

        public static void LogWrite(string fileName, string logBlock, bool endLog = false)
        {
            StreamWriter logFile = File.AppendText(fileName);
            if (endLog==true)
            {
                logFile.WriteLine(logBlock);
            }
            else
            {
                logFile.Write(logBlock);
            }
            logFile.Close();
        }

        public static void SendErrorLogs()
        {

        }
        public static void SendLog(string localFile, string serverFolder)
        {
            if (File.Exists(localFile))
            {
                if (serverFolder.Substring(serverFolder.Length - 1) != "\\") { serverFolder += "\\"; }
                try
                {
                    StreamReader srLocalFile = new StreamReader(localFile);
                    StreamWriter serverFile = new StreamWriter(serverFolder + localFile, append: true);
                    serverFile.Write(srLocalFile.ReadToEnd());
                    serverFile.Close();
                    srLocalFile.Close();
                    File.Delete(localFile);
                    //trasferimento file singoli
                    string[] files = Directory.GetFiles(".\\", "*.txt", SearchOption.TopDirectoryOnly);

                    foreach (string s in files)
                    {
                        if (!File.Exists(serverFolder + s))
                            File.Delete(serverFolder + s);
                        File.Move(s, serverFolder + s);
                    }
                }
                catch (System.IO.IOException e)
                {
                    string answer = e.Message;
                }
            }
        }
    }
}

